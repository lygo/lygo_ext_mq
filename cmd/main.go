package main

import (
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_mq/mq/mq_commons"
	"flag"
	"fmt"
	"os"
)

// ---------------------------------------------------------------------------------------------------------------------
//	m a i n
// ---------------------------------------------------------------------------------------------------------------------

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] APPLICATION %s ERROR: %s", mq_commons.Name, r)
			fmt.Println(message)
		}
	}()

	//-- command flags --//
	// publish
	cmdPublish := flag.NewFlagSet("publish", flag.ExitOnError)
	cmdPublishSettings := cmdPublish.String("settings", lygo_paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	cmdPublishDir := cmdPublish.String("dir", lygo_paths.Absolute("./uploads"), "Set directory with post to publish")
	// stub
	cmdStub := flag.NewFlagSet("stub", flag.ExitOnError)
	cmdStubSettings := cmdStub.String("settings", lygo_paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	cmdStubDir := cmdStub.String("dir", "./uploads", "Set target directory")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "publish":
			// BUILD
			_ = cmdPublish.Parse(os.Args[2:])
			dirWork := lygo_paths.Dir(*cmdPublishSettings)
			// set paths
			lygo_paths.GetWorkspace("*").SetPath(dirWork)
			// run command
			publish(*cmdPublishSettings, *cmdPublishDir)
		case "stub":
			// STUB
			_ = cmdStub.Parse(os.Args[2:])
			dirWork := lygo_paths.Dir(*cmdStubSettings)
			// set paths
			lygo_paths.GetWorkspace("*").SetPath(dirWork)
			dir := *cmdStubDir
			if len(dir) == 0 {
				dir = "./uploads"
			}
			// run command
			//stub(*cmdStubSettings, dir)
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func publish(file, dir string) {

}


