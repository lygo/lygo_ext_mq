package mq_commons

import "errors"

const(
	Name = "MQ"
	Version = "0.1.0"

	ProtocolAmqp = "amqp"
)

var(
	ErrorNilInstance= errors.New("nil_instance_error")
	ErrorUnsupportedProtocol = errors.New("unsupported_protocol_error")
	ErrorMissingConfiguration = errors.New("missing_configuration_error")
	ErrorMissingConnection = errors.New("missing_connection_error")
	ErrorConnectionIsClosed = errors.New("closed_connection_error")
)
