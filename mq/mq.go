package mq

import (
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_mq/mq/mq_commons"
	"bitbucket.org/lygo/lygo_ext_mq/mq/mq_drivers"
	"fmt"
)

// Build return a client implementation if supported
// config: ex {"protocol":"amqp","secret":"1234","url":"amqp://test:test@localhost:5672/"}
func Build(config interface{}) (mq_drivers.IDriver, error) {
	if s, b := config.(string); b {
		return buildFromString(s)
	} else {
		return buildFromString(lygo_json.Stringify(config))
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func buildFromString(config string) (mq_drivers.IDriver, error) {
	var dc *mq_drivers.DriverConfig
	err := lygo_json.Read(config, &dc)
	if nil != err {
		return nil, err
	}
	switch dc.Protocol {
	case mq_commons.ProtocolAmqp:
		// amqp client
		return mq_drivers.NewDriverAmqpFromString(config)
	default:
		return nil, lygo_errors.Prefix(mq_commons.ErrorUnsupportedProtocol, fmt.Sprintf("'%v': ", dc.Protocol))
	}
}
