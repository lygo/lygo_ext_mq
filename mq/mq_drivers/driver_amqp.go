package mq_drivers

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_commons/lygo_crypto"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_fmt"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"bitbucket.org/lygo/lygo_commons/lygo_stoppable"
	"bitbucket.org/lygo/lygo_ext_mq/mq/mq_commons"
	"fmt"
	"github.com/streadway/amqp"
	"time"
)

// RabbitMQ client implementation.
// https://tutorialedge.net/golang/go-rabbitmq-tutorial/
// https://www.rabbitmq.com/tutorials/tutorial-four-go.html
// DOCKER IMAGE: https://hub.docker.com/_/rabbitmq

// ---------------------------------------------------------------------------------------------------------------------
//	DriverAmqpConfig
// ---------------------------------------------------------------------------------------------------------------------

type DriverAmqpConfig struct {
	DriverConfig
	Url string `json:"url"`
}

type QueueAmqpConfig struct {
	Name       string                 `json:"name"`
	Durable    bool                   `json:"durable"`
	Exclusive  bool                   `json:"exclusive"`
	AutoDelete bool                   `json:"auto-delete"`
	NoWait     bool                   `json:"no-wait"`
	Arguments  map[string]interface{} `json:"args"`
	IsPassive  bool                   `json:"passive"`
}

type QueueBindAmqpConfig struct {
	Name      string                 `json:"name"`
	Key       string                 `json:"key"`
	Exchange  string                 `json:"exchange"`
	NoWait    bool                   `json:"no-wait"`
	Arguments map[string]interface{} `json:"args"`
}

type ExchangeAmqpConfig struct {
	Name       string                 `json:"name"`
	Kind       string                 `json:"kind"`
	Durable    bool                   `json:"durable"`
	Internal   bool                   `json:"internal"`
	AutoDelete bool                   `json:"auto-delete"`
	NoWait     bool                   `json:"no-wait"`
	Arguments  map[string]interface{} `json:"args"`
	IsPassive  bool                   `json:"passive"`
}

type ExchangeBindAmqpConfig struct {
	Destination string                 `json:"destination"`
	Key         string                 `json:"key"`
	Source      string                 `json:"source"`
	NoWait      bool                   `json:"no-wait"`
	Arguments   map[string]interface{} `json:"args"`
}

// ---------------------------------------------------------------------------------------------------------------------
//	DriverAmqp
// ---------------------------------------------------------------------------------------------------------------------

type DriverAmqp struct {
	config                  *DriverAmqpConfig
	queues                  map[string]amqp.Queue
	connectionCloseCallback func()

	__conn *amqp.Connection // late initialized
}

func NewDriverAmqpFromString(text string) (*DriverAmqp, error) {
	config, err := NewDriverAmqpConfig(text)
	if nil != err {
		return nil, err
	}
	return NewDriverAmqp(config)
}

func NewDriverAmqp(config *DriverAmqpConfig) (*DriverAmqp, error) {
	instance := new(DriverAmqp)
	instance.config = config
	instance.queues = make(map[string]amqp.Queue)

	return instance, nil
}

func NewDriverAmqpConfig(text string) (*DriverAmqpConfig, error) {
	var response *DriverAmqpConfig
	err := lygo_json.Read(text, &response)
	if nil != err {
		return nil, err
	}
	return response, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	DriverAmqp   p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DriverAmqp) GoString() string {
	if nil != instance {
		return instance.String()
	}
	return ""
}

func (instance *DriverAmqp) String() string {
	if nil != instance {
		return lygo_json.Stringify(instance.config)
	}
	return ""
}

func (instance *DriverAmqp) Close() error {
	if nil != instance && nil != instance.__conn && !instance.__conn.IsClosed() {
		err := instance.__conn.Close()
		instance.__conn = nil
		return err
	}
	return nil
}

func (instance *DriverAmqp) Info() string {
	if nil != instance {
		if c, err := instance.connection(); nil == err {
			return lygo_fmt.FormatMap(c.Properties)
		} else {
			return err.Error()
		}

	}
	return ""
}

func (instance *DriverAmqp) Ping() (bool, error) {
	if nil != instance {
		if _, err := instance.connection(); nil == err {
			return true, nil
		} else {
			return false, err
		}
	}
	return false, mq_commons.ErrorNilInstance
}

func (instance *DriverAmqp) ExchangeDeclare(settings interface{}) error {
	var config *ExchangeAmqpConfig
	err := lygo_json.Read(lygo_json.Stringify(settings), &config)
	if nil != err {
		return err
	}
	// create a channel
	if connection, connErr := instance.connection(); nil == connErr {
		channel, chanErr := connection.Channel()
		if nil != chanErr {
			return chanErr
		}
		defer channel.Close()

		if config.IsPassive {
			// declare an exchange
			err = channel.ExchangeDeclarePassive(
				config.Name,
				config.Kind,
				config.Durable,
				config.AutoDelete,
				config.Internal,
				config.NoWait,
				config.Arguments,
			)
		} else {
			// declare an exchange
			err = channel.ExchangeDeclare(
				config.Name,
				config.Kind,
				config.Durable,
				config.AutoDelete,
				config.Internal,
				config.NoWait,
				config.Arguments,
			)
		}

		return err
	} else {
		return connErr
	}
}

func (instance *DriverAmqp) ExchangeDelete(name string, args ...interface{}) error {
	if connection, connErr := instance.connection(); nil == connErr {
		channel, err := connection.Channel()
		if nil != err {
			return err
		}
		defer channel.Close()
		ifUnused := lygo.Convert.ToBool(lygo.Arrays.GetAt(args, 0, false))
		noWait := lygo.Convert.ToBool(lygo.Arrays.GetAt(args, 1, false))
		err = channel.ExchangeDelete(name, ifUnused, noWait)
		if nil != err {
			return err
		}
		return nil
	} else {
		return connErr
	}
}

/*
ExchangeBind binds an exchange to another exchange to create inter-exchange
routing topologies on the server.  This can decouple the private topology and
routing exchanges from exchanges intended solely for publishing endpoints.

Binding two exchanges with identical arguments will not create duplicate
bindings.

Binding one exchange to another with multiple bindings will only deliver a
message once.  For example if you bind your exchange to `amq.fanout` with two
different binding keys, only a single message will be delivered to your
exchange even though multiple bindings will match.

Given a message delivered to the source exchange, the message will be forwarded
to the destination exchange when the routing key is matched.

  ExchangeBind("sell", "MSFT", "trade", false, nil)
  ExchangeBind("buy", "AAPL", "trade", false, nil)

  Delivery       Source      Key      Destination
  example        exchange             exchange
  -----------------------------------------------
  key: AAPL  --> trade ----> MSFT     sell
                       \---> AAPL --> buy

When noWait is true, do not wait for the server to confirm the binding.  If any
error occurs the channel will be closed.  Add a listener to NotifyClose to
handle these errors.

Optional arguments specific to the exchanges bound can also be specified.
*/
func (instance *DriverAmqp) ExchangeBind(settings interface{}) error {
	if connection, connErr := instance.connection(); nil == connErr {
		var config *ExchangeBindAmqpConfig
		err := lygo_json.Read(lygo_json.Stringify(settings), &config)
		if nil != err {
			return err
		}

		channel, err := connection.Channel()
		if nil != err {
			return err
		}
		defer channel.Close()

		err = channel.ExchangeBind(config.Source, config.Key, config.Destination, config.NoWait, config.Arguments)
		return err
	} else {
		return connErr
	}
}

func (instance *DriverAmqp) ExchangeUnbind(settings interface{}) error {
	if connection, connErr := instance.connection(); nil == connErr {
		var config *ExchangeBindAmqpConfig
		err := lygo_json.Read(lygo_json.Stringify(settings), &config)
		if nil != err {
			return err
		}

		channel, err := connection.Channel()
		if nil != err {
			return err
		}
		defer channel.Close()

		err = channel.ExchangeUnbind(config.Source, config.Key, config.Destination, config.NoWait, config.Arguments)
		return err
	} else {
		return connErr
	}
}

func (instance *DriverAmqp) QueueDeclare(settings interface{}) (interface{}, error) {
	var config *QueueAmqpConfig
	err := lygo_json.Read(lygo_json.Stringify(settings), &config)
	if nil != err {
		return nil, err
	}

	// create a channel
	if connection, err := instance.connection(); nil == err {
		channel, err := connection.Channel()
		if nil != err {
			return nil, err
		}
		defer channel.Close()

		// declare a queue
		var q amqp.Queue
		var decErr error
		if config.IsPassive {
			q, decErr = channel.QueueDeclarePassive(
				config.Name,
				config.Durable,
				config.AutoDelete,
				config.Exclusive,
				config.NoWait,
				config.Arguments,
			)
		} else {
			q, decErr = channel.QueueDeclare(
				config.Name,
				config.Durable,
				config.AutoDelete,
				config.Exclusive,
				config.NoWait,
				config.Arguments,
			)
		}
		if nil != decErr {
			return nil, decErr
		}
		// add queue to list
		instance.queues[q.Name] = q
		return queueToMap(q), nil
	} else {
		return nil, err
	}
}

/*
QueueDelete removes the queue from the server including all bindings then
purges the messages based on server configuration, returning the number of
messages purged.
*/
func (instance *DriverAmqp) QueueDelete(name string, args ...interface{}) (int, error) {
	if connection, err := instance.connection(); nil == err {
		channel, err := connection.Channel()
		if nil != err {
			return 0, err
		}
		defer channel.Close()
		ifUnused := lygo.Convert.ToBool(lygo.Arrays.GetAt(args, 0, false))
		ifEmpty := lygo.Convert.ToBool(lygo.Arrays.GetAt(args, 1, false))
		noWait := lygo.Convert.ToBool(lygo.Arrays.GetAt(args, 2, false))
		count, err := channel.QueueDelete(name, ifUnused, ifEmpty, noWait)
		if nil != err {
			return 0, err
		}
		return count, nil
	} else {
		return 0, err
	}
}

/*
QueueBind binds an exchange to a queue so that publishings to the exchange will
be routed to the queue when the publishing routing key matches the binding
routing key.

  QueueBind("pagers", "alert", "log", false, nil)
  QueueBind("emails", "info", "log", false, nil)

  Delivery       Exchange  Key       Queue
  -----------------------------------------------
  key: alert --> log ----> alert --> pagers
  key: info ---> log ----> info ---> emails
  key: debug --> log       (none)    (dropped)

If a binding with the same key and arguments already exists between the
exchange and queue, the attempt to rebind will be ignored and the existing
binding will be retained.

In the case that multiple bindings may cause the message to be routed to the
same queue, the server will only route the publishing once.  This is possible
with topic exchanges.

  QueueBind("pagers", "alert", "amq.topic", false, nil)
  QueueBind("emails", "info", "amq.topic", false, nil)
  QueueBind("emails", "#", "amq.topic", false, nil) // match everything

  Delivery       Exchange        Key       Queue
  -----------------------------------------------
  key: alert --> amq.topic ----> alert --> pagers
  key: info ---> amq.topic ----> # ------> emails
                           \---> info ---/
  key: debug --> amq.topic ----> # ------> emails

It is only possible to bind a durable queue to a durable exchange regardless of
whether the queue or exchange is auto-deleted.  Bindings between durable queues
and exchanges will also be restored on server restart.

If the binding could not complete, an error will be returned and the channel
will be closed.

When noWait is false and the queue could not be bound, the channel will be
closed with an error.

*/
func (instance *DriverAmqp) QueueBind(settings interface{}) error {
	if connection, connErr := instance.connection(); nil == connErr {
		var config *QueueBindAmqpConfig
		err := lygo_json.Read(lygo_json.Stringify(settings), &config)
		if nil != err {
			return err
		}

		channel, err := connection.Channel()
		if nil != err {
			return err
		}
		defer channel.Close()

		err = channel.QueueBind(config.Name, config.Key, config.Exchange, config.NoWait, config.Arguments)
		return err
	} else {
		return connErr
	}
}

/*
QueueUnbind removes a binding between an exchange and queue matching the key and
arguments.

It is possible to send and empty string for the exchange name which means to
unbind the queue from the default exchange.
*/
func (instance *DriverAmqp) QueueUnbind(settings interface{}) error {
	if connection, connErr := instance.connection(); nil == connErr {
		var config *QueueBindAmqpConfig
		err := lygo_json.Read(lygo_json.Stringify(settings), &config)
		if nil != err {
			return err
		}

		channel, err := connection.Channel()
		if nil != err {
			return err
		}
		defer channel.Close()

		err = channel.QueueUnbind(config.Name, config.Key, config.Exchange, config.Arguments)
		return err
	} else {
		return connErr
	}
}

func (instance *DriverAmqp) QueueInspect(name string) (interface{}, error) {
	if connection, err := instance.connection(); nil == err {
		channel, err := connection.Channel()
		if nil != err {
			return nil, err
		}
		defer channel.Close()
		q, err := channel.QueueInspect(name)
		if nil != err {
			return nil, err
		}
		return queueToMap(q), nil
	} else {
		return nil, err
	}
}

/*
QueuePurge removes all messages from the named queue which are not waiting to
be acknowledged.  Messages that have been delivered but have not yet been
acknowledged will not be removed.

When successful, returns the number of messages purged.
*/
func (instance *DriverAmqp) QueuePurge(name string, args ...interface{}) (int, error) {
	if connection, err := instance.connection(); nil == err {
		channel, err := connection.Channel()
		if nil != err {
			return 0, err
		}
		defer channel.Close()
		noWait := lygo.Convert.ToBool(lygo.Arrays.GetAt(args, 0, false))
		count, err := channel.QueuePurge(name, noWait)
		if nil != err {
			return 0, err
		}
		return count, nil
	} else {
		return 0, err
	}
}

func (instance *DriverAmqp) RpcCommand(rpcChannel string, emitterSettings, listenerSettings interface{}, rawMessage interface{}, callback ListenerHandler, timeout time.Duration) error {
	if nil != instance && nil != callback {
		if connection, err := instance.connection(); nil == err {
			// creates a correlationId
			corrId := lygo_rnd.RndChars(32)

			// creates a channel
			var channel *amqp.Channel
			channel, err = connection.Channel()
			if nil != err {
				return lygo_errors.Prefix(err, "Error retrieving channel: ")
			}

			// parse settings for both emitter and listener
			eSettings, err := toEmitterSettings(emitterSettings)
			if nil != err {
				return lygo_errors.Prefix(err, "Error parsing emitter settings: ")
			}
			if len(eSettings.Key) == 0 {
				eSettings.Key = rpcChannel
			}
			lSettings, err := toListenerSettings(listenerSettings)
			if nil != err {
				return lygo_errors.Prefix(err, "Error parsing listener settings: ")
			}

			// check if an exclusive queue is required
			if len(lSettings.Queue) == 0 {
				// declare an exclusive queue from same channel
				q, err := channel.QueueDeclare(
					"",    // name
					false, // durable
					false, // delete when unused
					true,  // exclusive
					false, // noWait
					nil,   // arguments
				)
				if nil != err {
					return lygo_errors.Prefix(err, "Error declaring queue: ")
				}
				lSettings.Queue = q.Name // uses queue name for consumer
			}

			// add rpcChannel references
			eSettings.Key = rpcChannel

			// prepare listener
			handler := NewListenerAmqpHandler()
			msgs, err := channel.Consume(
				lSettings.Queue,
				lSettings.ConsumerTag,
				lSettings.AutoAck,
				lSettings.Exclusive,
				lSettings.NoLocal,
				lSettings.NoWait,
				lSettings.Arguments,
			)
			if nil != err {
				return lygo_errors.Prefix(err, "Error consuming message: ")
			}

			// decode the message to send
			publishing, err := createPublishing(rawMessage)
			if nil != err {
				return lygo_errors.Prefix(err, "Error decoding message: ")
			}
			if len(instance.config.Secret) > 0 {
				data, err := lygo_crypto.EncryptTextWithPrefix(string(publishing.Body), []byte(instance.config.Secret))
				if nil == err {
					publishing.Body = []byte(data)
				}
			}
			publishing.CorrelationId = corrId
			publishing.ReplyTo = lSettings.Queue

			// publish the message
			err = channel.Publish(
				eSettings.Exchange,
				eSettings.Key,
				eSettings.Mandatory,
				eSettings.Immediate,
				publishing,
			)
			if nil != err {
				return lygo_errors.Prefix(err, "Error publishing message: ")
			}

			go handler.handle(channel, msgs, corrId, instance.config.Secret, !lSettings.AutoAck, func(message map[string]interface{}) {
				// invoke callback
				callback(message)
				// close and exit
				handler.Close()
			})

			// wait until response or timeout
			handler.JoinTimeout(timeout)
			_ = channel.Close()
			return nil
		} else {
			return err
		}
	}
	return mq_commons.ErrorNilInstance
}

// NewEmitter generate new emitter
func (instance *DriverAmqp) NewEmitter(settings interface{}) (IEmitter, error) {
	if nil != instance {
		if connection, err := instance.connection(); nil == err {
			return newEmitterAmqp(connection, instance.config.Secret, settings)
		} else {
			return nil, err
		}
	}
	return nil, mq_commons.ErrorNilInstance
}

// NewListener generate new message listener/consumer
func (instance *DriverAmqp) NewListener(settings interface{}) (IListener, error) {
	if nil != instance {
		if connection, err := instance.connection(); nil == err {
			return newListenerAmqp(connection, instance.config.Secret, settings)
		} else {
			return nil, err
		}
	}
	return nil, mq_commons.ErrorNilInstance
}

func (instance *DriverAmqp) NotifyDisconnection(connectionCloseCallback func()) {
	if nil != instance {
		instance.connectionCloseCallback = connectionCloseCallback
	}
}

func (instance *DriverAmqp) connection() (*amqp.Connection, error) {
	// new connection
	if nil == instance.__conn {
		if connection, err := connect(instance.config.Url); nil == err {
			instance.__conn = connection
			go func() {
				<-connection.NotifyClose(make(chan *amqp.Error)) //Listen to NotifyClose
				instance.__conn = nil
				if nil != instance.connectionCloseCallback {
					instance.connectionCloseCallback()
				}
			}()
		} else {
			return nil, err
		}
	} else if instance.__conn.IsClosed() {
		return nil, mq_commons.ErrorConnectionIsClosed
	}
	return instance.__conn, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	EmitterAmqp (publisher)
// ---------------------------------------------------------------------------------------------------------------------

// EmitterAmqpSettings configuration for emitter
type EmitterAmqpSettings struct {
	Exchange  string `json:"exchange"`
	Key       string `json:"key"`
	Mandatory bool   `json:"mandatory"`
	Immediate bool   `json:"immediate"`
}

// EmitterAmqpMessage message to send
type EmitterAmqpMessage struct {
	// Application or exchange specific fields,
	// the headers exchange will inspect this field.
	Headers map[string]interface{} `json:"headers,omitempty"`

	// Properties
	ContentType     string    `json:"content-type,omitempty"`     // MIME content type
	ContentEncoding string    `json:"content-encoding,omitempty"` // MIME content encoding
	DeliveryMode    uint8     `json:"delivery-mode,omitempty"`    // Transient (0 or 1) or Persistent (2)
	Priority        uint8     `json:"priority,omitempty"`         // 0 to 9
	CorrelationId   string    `json:"correlation-id,omitempty"`   // correlation identifier
	ReplyTo         string    `json:"reply-to,omitempty"`         // address to reply to (ex: RPC)
	Expiration      string    `json:"expiration,omitempty"`       // message expiration spec
	MessageId       string    `json:"message-id,omitempty"`       // message identifier
	Timestamp       time.Time `json:"timestamp,omitempty"`        // message timestamp
	Type            string    `json:"type,omitempty"`             // message type name
	UserId          string    `json:"user-id,omitempty"`          // creating user id - ex: "guest"
	AppId           string    `json:"app-id,omitempty"`           // creating application id

	// The application specific payload of the message
	Body []byte `json:"body"`
}

type EmitterAmqp struct {
	conn     *amqp.Connection
	secret   string
	settings *EmitterAmqpSettings
}

func newEmitterAmqp(connection *amqp.Connection, secret string, settings interface{}) (IEmitter, error) {
	instance := new(EmitterAmqp)
	instance.conn = connection
	instance.secret = secret

	s, err := toEmitterSettings(settings)
	if nil != err {
		return nil, err
	}
	instance.settings = s

	return instance, nil
}

func (instance *EmitterAmqp) GoString() string {
	if nil != instance {
		return instance.String()
	}
	return ""
}

func (instance *EmitterAmqp) String() string {
	if nil != instance {
		return lygo_json.Stringify(instance.settings)
	}
	return ""
}

func (instance *EmitterAmqp) Close() error {
	if nil != instance {
		if nil != instance.conn && !instance.conn.IsClosed() {
			err := instance.conn.Close()
			instance.conn = nil
			return err
		}
		return nil
	}
	return mq_commons.ErrorNilInstance
}

func (instance *EmitterAmqp) Emit(rawMessage interface{}) error {
	if nil != instance {
		if connection, err := instance.connection(); nil == err {
			// decode message
			publishing, err := createPublishing(rawMessage)
			if nil != err {
				return err
			}

			// create a channel
			channel, err := connection.Channel()
			if nil != err {
				return err
			}
			defer channel.Close()

			if len(instance.secret) > 0 {
				data, err := lygo_crypto.EncryptTextWithPrefix(string(publishing.Body), []byte(instance.secret))
				if nil == err {
					publishing.Body = []byte(data)
				}
			}

			err = channel.Publish(instance.settings.Exchange, instance.settings.Key, instance.settings.Mandatory,
				instance.settings.Immediate, publishing)
			if nil != err {
				return err
			}
			return nil
		} else {
			return err
		}
	}
	return mq_commons.ErrorNilInstance
}

func (instance *EmitterAmqp) connection() (*amqp.Connection, error) {
	if nil == instance.conn || instance.conn.IsClosed() {
		return nil, mq_commons.ErrorMissingConnection
	}
	return instance.conn, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	ListenerAmqp (consumer)
// ---------------------------------------------------------------------------------------------------------------------

type ListenerAmqpSettings struct {
	Queue       string                 `json:"queue"`
	ConsumerTag string                 `json:"consumer-tag"`
	NoLocal     bool                   `json:"no-local"`
	AutoAck     bool                   `json:"auto-ack"`
	Exclusive   bool                   `json:"exclusive"`
	NoWait      bool                   `json:"no-wait"`
	Arguments   map[string]interface{} `json:"args"`
}

type ListenerAmqpMessage struct {
	// Application or exchange specific fields,
	// the headers exchange will inspect this field.
	Headers map[string]interface{} `json:"headers,omitempty"`

	// Properties
	ContentType     string    `json:"content-type,omitempty"`     // MIME content type
	ContentEncoding string    `json:"content-encoding,omitempty"` // MIME content encoding
	DeliveryMode    uint8     `json:"delivery-mode,omitempty"`    // Transient (0 or 1) or Persistent (2)
	Priority        uint8     `json:"priority,omitempty"`         // 0 to 9
	CorrelationId   string    `json:"correlation-id,omitempty"`   // correlation identifier
	ReplyTo         string    `json:"reply-to,omitempty"`         // address to reply to (ex: RPC)
	Expiration      string    `json:"expiration,omitempty"`       // message expiration spec
	MessageId       string    `json:"message-id,omitempty"`       // message identifier
	Timestamp       time.Time `json:"timestamp,omitempty"`        // message timestamp
	Type            string    `json:"type,omitempty"`             // message type name
	UserId          string    `json:"user-id,omitempty"`          // creating user id - ex: "guest"
	AppId           string    `json:"app-id,omitempty"`           // creating application id

	// Valid only with Channel.Consume
	ConsumerTag string `json:"consumer-tag,omitempty"`

	// Valid only with Channel.Get
	MessageCount uint32 `json:"message-count,omitempty"`

	DeliveryTag uint64 `json:"delivery-tag,omitempty"`
	Redelivered bool   `json:"redelivered"`
	Exchange    string `json:"exchange,omitempty"`    // basic.publish exchange
	RoutingKey  string `json:"routing-key,omitempty"` // basic.publish routing key

	// The application specific payload of the message
	Body []byte `json:"body"`
}

type ListenerAmqp struct {
	conn     *amqp.Connection
	secret   string
	settings *ListenerAmqpSettings
	handler  *ListenerAmqpHandler
}

func newListenerAmqp(connection *amqp.Connection, secret string, settings interface{}) (IListener, error) {
	instance := new(ListenerAmqp)
	instance.conn = connection
	instance.secret = secret
	instance.handler = NewListenerAmqpHandler()

	s, err := toListenerSettings(settings)
	if nil != err {
		return nil, err
	}
	instance.settings = s

	return instance, nil
}

func (instance *ListenerAmqp) GoString() string {
	if nil != instance {
		return instance.String()
	}
	return ""
}

func (instance *ListenerAmqp) String() string {
	if nil != instance {
		m := map[string]interface{}{}
		return lygo_json.Stringify(m)
	}
	return ""
}

func (instance *ListenerAmqp) Close() error {
	if nil != instance {
		if nil != instance.handler {
			instance.handler.Close()
		}
		if nil != instance.conn && !instance.conn.IsClosed() {
			err := instance.conn.Close()
			instance.conn = nil

			return err
		}
	}
	return mq_commons.ErrorNilInstance
}

func (instance *ListenerAmqp) Listen(callback ListenerHandler) error {
	if nil != instance {
		if connection, err := instance.connection(); nil == err {

			// create a channel
			channel, err := connection.Channel()
			if nil != err {
				return err
			}

			msgs, err := channel.Consume(
				instance.settings.Queue,
				instance.settings.ConsumerTag,
				instance.settings.AutoAck,
				instance.settings.Exclusive,
				instance.settings.NoLocal,
				instance.settings.NoWait,
				instance.settings.Arguments,
			)
			if nil != err {
				return err
			}

			go instance.handler.handle(channel, msgs, "", instance.secret, !instance.settings.AutoAck, callback)

			return nil
		}
		return mq_commons.ErrorMissingConnection
	}
	return mq_commons.ErrorNilInstance
}

func (instance *ListenerAmqp) Join() {
	// locks and wait for exit response
	if nil != instance.handler {
		instance.handler.Join()
	}
}

func (instance *ListenerAmqp) JoinTimeout(d time.Duration) {
	// locks and wait for exit response
	if nil != instance.handler {
		instance.handler.JoinTimeout(d)
	}
}

func (instance *ListenerAmqp) connection() (*amqp.Connection, error) {
	if nil == instance.conn || instance.conn.IsClosed() {
		return nil, mq_commons.ErrorMissingConnection
	}
	return instance.conn, nil
}

func (instance *ListenerAmqp) isClosed() bool {
	if nil != instance && nil != instance.conn {
		return instance.conn.IsClosed()
	}
	return true
}

// ---------------------------------------------------------------------------------------------------------------------
//	listener handler
// ---------------------------------------------------------------------------------------------------------------------

// ListenerAmqpHandler Utility message handler
type ListenerAmqpHandler struct {
	stoppable *lygo_stoppable.Stoppable
}

func NewListenerAmqpHandler() *ListenerAmqpHandler {
	instance := new(ListenerAmqpHandler)
	instance.stoppable = lygo_stoppable.NewStoppable()
	instance.stoppable.Start()
	return instance
}

func (instance *ListenerAmqpHandler) Close() {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Stop()
	}
}

func (instance *ListenerAmqpHandler) Join() {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Join()
	}
}

func (instance *ListenerAmqpHandler) JoinTimeout(d time.Duration) {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.JoinTimeout(d)
	}
}

func (instance *ListenerAmqpHandler) isClosed() bool {
	if nil != instance {
		return nil == instance.stoppable || instance.stoppable.IsStopped()
	}
	return true
}

func (instance *ListenerAmqpHandler) handle(channel *amqp.Channel, msgs <-chan amqp.Delivery, correlationId, secret string, doAck bool, callback ListenerHandler) {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			fmt.Println(fmt.Sprintf("ERROR in ListenerAmqpHandler.handle: %v", r))
			if nil != instance {
				instance.Close()
			}
		}
	}()
	for {
		if nil != instance && !instance.isClosed() && nil != callback {
			select {
			case d := <-msgs:
				// message
				if nil != instance && !instance.isClosed() &&
					nil != callback && (len(correlationId) == 0 || correlationId == d.CorrelationId) {
					// fmt.Println("Received Message: ", string(d.Body))
					m := lygo.Convert.ToMap(convertListenerMessage(d))
					m["body"] = string(d.Body)
					if len(secret) > 0 {
						data, err := lygo_crypto.DecryptTextWithPrefix(string(d.Body), []byte(secret))
						if nil == err {
							m["body"] = data
						}
					}
					callback(m)
					if doAck {
						_ = d.Ack(false)
					}
				}
			}
		} else {
			goto closeChannel
		}
	}
closeChannel:
	if nil != channel {
		_ = channel.Close()
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func connect(url string) (*amqp.Connection, error) {
	if len(url) > 0 {
		return amqp.Dial(url)
	}
	return nil, mq_commons.ErrorMissingConfiguration
}

func queueToMap(q amqp.Queue) map[string]interface{} {
	return map[string]interface{}{
		"name":      q.Name,
		"messages":  q.Messages,
		"consumers": q.Consumers,
	}
}

func toEmitterSettings(settings interface{}) (*EmitterAmqpSettings, error) {
	var s *EmitterAmqpSettings
	if nil != settings {
		err := lygo_json.Read(lygo_json.Stringify(settings), &s)
		if nil != err {
			return nil, err
		}
	} else {
		s = &EmitterAmqpSettings{
			Exchange:  "",
			Key:       "default",
			Mandatory: false,
			Immediate: false,
		}
	}
	return s, nil
}

func toListenerSettings(settings interface{}) (*ListenerAmqpSettings, error) {
	var s *ListenerAmqpSettings
	if nil != settings {
		err := lygo_json.Read(lygo_json.Stringify(settings), &s)
		if nil != err {
			return nil, err
		}
	} else {
		s = &ListenerAmqpSettings{
			Queue:       "",
			ConsumerTag: "",
			NoLocal:     false,
			AutoAck:     false,
			Exclusive:   false,
			NoWait:      false,
			Arguments:   nil,
		}
	}
	return s, nil
}

func convertListenerMessage(d amqp.Delivery) *ListenerAmqpMessage {
	m := new(ListenerAmqpMessage)

	m.Headers = d.Headers

	// Properties
	m.ContentType = d.ContentType
	m.ContentEncoding = d.ContentEncoding
	m.DeliveryMode = d.DeliveryMode
	m.Priority = d.Priority
	m.CorrelationId = d.CorrelationId
	m.ReplyTo = d.ReplyTo
	m.Expiration = d.Expiration
	m.MessageId = d.MessageId
	m.Timestamp = d.Timestamp
	m.Type = d.Type
	m.UserId = d.UserId
	m.AppId = d.AppId

	// Valid only with Channel.Consume
	m.ConsumerTag = d.ConsumerTag

	// Valid only with Channel.Get
	m.MessageCount = d.MessageCount

	m.DeliveryTag = d.DeliveryTag
	m.Redelivered = d.Redelivered
	m.Exchange = d.Exchange
	m.RoutingKey = d.RoutingKey

	m.Body = d.Body

	return m
}

func createPublishing(rawMessage interface{}) (amqp.Publishing, error) {
	var publishing amqp.Publishing
	// decode message
	var message *EmitterAmqpMessage
	err := lygo_json.Read(lygo_json.Stringify(rawMessage), &message)
	if nil != err {
		return publishing, err
	}

	// convert message for publishing
	publishing = amqp.Publishing{
		Headers:         message.Headers,
		ContentType:     message.ContentType,
		ContentEncoding: message.ContentEncoding,
		DeliveryMode:    message.DeliveryMode,
		Priority:        message.Priority,
		CorrelationId:   message.CorrelationId,
		ReplyTo:         message.ReplyTo,
		Expiration:      message.Expiration,
		MessageId:       message.MessageId,
		Timestamp:       message.Timestamp,
		Type:            message.Type,
		UserId:          message.UserId,
		AppId:           message.AppId,
		Body:            message.Body,
	}

	if len(publishing.ContentType) == 0 {
		publishing.ContentType = "text/plain"
	}
	if publishing.Timestamp.IsZero() {
		publishing.Timestamp = time.Now()
	}
	return publishing, nil
}
