package mq_drivers

import "time"

type DriverConfig struct {
	Protocol string `json:"protocol"` // amqp, ...
	Secret   string `json:"secret"`
}

// IDriver connection
type IDriver interface {
	Close() error
	Ping() (bool, error) // test connection
	Info() string        // printable properties

	QueueDeclare(settings interface{}) (interface{}, error)    // queue declare
	QueueDelete(name string, args ...interface{}) (int, error) // queue delete
	QueueInspect(name string) (interface{}, error)             // queue inspect
	QueuePurge(name string, args ...interface{}) (int, error)  // queue clean
	QueueBind(settings interface{}) error
	QueueUnbind(settings interface{}) error

	ExchangeDeclare(settings interface{}) error
	ExchangeDelete(name string, args ...interface{}) error
	ExchangeBind(settings interface{}) error
	ExchangeUnbind(settings interface{}) error

	RpcCommand(rpcChannel string, emitterSettings, listenerSettings interface{}, rawMessage interface{}, callback ListenerHandler, timeout time.Duration) error

	NewEmitter(settings interface{}) (IEmitter, error)   // send messages
	NewListener(settings interface{}) (IListener, error) // consume messages

	NotifyDisconnection(connectionCloseCallback func())
}

// IEmitter send/publish messages
type IEmitter interface {
	Close() error
	Emit(rawSignal interface{}) error
}

// IListener listen/consume messages
type IListener interface {
	Close() error
	Listen(ListenerHandler) error
	Join()
	JoinTimeout(d time.Duration)
}

type ListenerHandler func(message map[string]interface{})
