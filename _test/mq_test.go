package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"bitbucket.org/lygo/lygo_ext_mq/mq"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"strconv"
	"testing"
	"time"
)

//  http://localhost:15672 user:guest, password:guest

func TestAMQP(t *testing.T) {
	// AMQP
	config := map[string]interface{}{
		"protocol": "amqp",
		"url":      "amqp://test:test@localhost:5672/",
		"secret":   "1234",
	}
	client, err := mq.Build(config)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(client)

	if b, err := client.Ping(); !b {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("CONNECTED TO BROKER")
	fmt.Println(client.Info())

	// creates/get queue
	q, err := client.QueueDeclare(map[string]interface{}{"name": "SampleQueue"})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("QUEUE STATUS: ", lygo_json.Stringify(q))

	//------ EMITTER ------
	emitter, err := client.NewEmitter(map[string]interface{}{"key": "SampleQueue"})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("EMITTER", emitter)

	// emit messages 2 per second
	go func() {
		count := 0
		for true {
			count++
			message := map[string]interface{}{
				"content-type": "text/plain",
				"body":         []byte(fmt.Sprintf("%v)\t[%v]\tHello AMQP!!!! ", count, time.Now())),
			}
			err = emitter.Emit(message)
			if nil != err {
				t.Error(err)
			}
			time.Sleep(time.Millisecond * 500)
		}
	}()

	//------ LISTENER ------
	listener, err := client.NewListener(map[string]interface{}{"queue": "SampleQueue"})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = listener.Listen(func(message map[string]interface{}) {
		fmt.Println("LISTENER message:", message["body"], lygo_json.Stringify(message))
	})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	//------ WAIT ------
	// wait 10 seconds before close
	time.Sleep(10 * time.Second)
	//listener.JoinTimeout(10 * time.Second)

	// ------ CLOSE ------
	// close first listener
	err = listener.Close()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	// close emitter
	time.Sleep(3 * time.Second)
	err = emitter.Close()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	q, err = client.QueueInspect("SampleQueue")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("QUEUE STATUS: ", lygo_json.Stringify(q))
	// remove messages
	count, err := client.QueuePurge("SampleQueue", false)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("REMOVED MESSAGES: ", count)

	q, err = client.QueueInspect("SampleQueue")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("QUEUE STATUS: ", lygo_json.Stringify(q))
}

func TestExchangeAMQP(t *testing.T) {
	const queue = "queue1"
	const key = "my-key"
	const exchange = "exchange-1"

	// AMQP
	config := map[string]interface{}{
		"protocol": "amqp",
		"url":      "amqp://test:test@localhost:5672/",
		"secret":   "1234",
	}
	client, err := mq.Build(config)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(client)

	if b, err := client.Ping(); !b {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("CONNECTED TO BROKER")
	fmt.Println(client.Info())

	// creates exchange
	err = client.ExchangeDeclare(map[string]interface{}{"name": exchange, "kind": "direct", "durable": true})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	_, err = client.QueueDeclare(map[string]interface{}{"name": queue, "exclusive": true})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = client.QueueBind(map[string]interface{}{"name": queue, "exchange": exchange, "key": key})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	//------ LISTENER ------
	listener, err := client.NewListener(map[string]interface{}{"queue": queue})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = listener.Listen(func(message map[string]interface{}) {
		fmt.Println("message:", message["body"], lygo_json.Stringify(message))
	})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	//------ EMITTER ------
	client2, err := mq.Build(config)
	emitter, err := client2.NewEmitter(map[string]interface{}{"exchange": exchange, "key": key})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("EMITTER", emitter)
	// declare  exchange
	err = client2.ExchangeDeclare(map[string]interface{}{"name": exchange, "kind": "direct", "durable": true})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	// emit messages 2 per second
	go func() {
		count := 0
		for true {
			count++
			message := map[string]interface{}{
				"content-type": "text/plain",
				"body":         []byte(fmt.Sprintf("%v)\t[%v]\tHello AMQP!!!! ", count, time.Now())),
			}
			err = emitter.Emit(message)
			if nil != err {
				t.Error(err)
			}
			time.Sleep(time.Millisecond * 500)
		}
	}()

	//------ WAIT ------
	// wait 10 seconds before close
	time.Sleep(10 * time.Second)

	// ------ CLOSE ------

	// close first listener
	err = listener.Close()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// close emitter
	time.Sleep(3 * time.Second)
	err = emitter.Close()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	_, err = client.QueueInspect(queue)
	if nil == err {
		t.Error("Expected closed_connection_error")
		t.FailNow()
	}
}

func TestRpc(t *testing.T) {
	queueName := "rpc_queue"

	go startRpcNativeServer(queueName)

	// AMQP
	config := map[string]interface{}{
		"protocol": "amqp",
		"url":      "amqp://test:test@localhost:5672/",
		"secret":   "",
	}
	driver, err := mq.Build(config)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(driver)

	driver.NotifyDisconnection(func() {
		fmt.Println("NotifyDisconnection", "DISCONNECTED")
	})


	// consumer
	// listenerSettings := map[string]interface{}{"auto-ack": true}
	// publisher
	// emitterSettings := map[string]interface{}{"key": queueName}
	message := map[string]interface{}{
		"content-type": "text/plain",
		"body":         []byte(fmt.Sprintf("%v", 0)),
	}
	err = driver.RpcCommand(queueName, nil, nil, message, func(message map[string]interface{}) {
		// fmt.Println("RESPONSE", message["body"])
		res := lygo_conv.ToInt(message["body"])
		log.Printf(" [.] Client got %d", res)
	}, 20*time.Second)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	_ = driver.Close()
}

func TestRpcNativeClient(t *testing.T) {
	queueName := "rpc_queue"

	// go startRpcNativeServer(queueName)
	go startRpcServer(queueName)

	conn, err := amqp.Dial("amqp://test:test@localhost:5672/")
	if nil != err {
		t.Error(err, "Failed to connect to RabbitMQ")
		t.FailNow()
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if nil != err {
		t.Error(err, "Failed to open a channel")
		t.FailNow()
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // noWait
		nil,   // arguments
	)
	if nil != err {
		t.Error(err, "Failed to declare a queue")
		t.FailNow()
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if nil != err {
		t.Error(err, "Failed to register a consumer")
		t.FailNow()
	}

	corrId := lygo_rnd.RndChars(32)

	err = ch.Publish(
		"",          // exchange
		queueName, // routing key
		false,       // mandatory
		false,       // immediate
		amqp.Publishing{
			ContentType:   "text/plain",
			CorrelationId: corrId,
			ReplyTo:       q.Name,
			Body:          []byte(fmt.Sprintf("%v", 0)),
		})
	if nil != err {
		t.Error(err, "Failed to publish a message")
		t.FailNow()
	}

	for d := range msgs {
		if corrId == d.CorrelationId {
			res, err := strconv.Atoi(string(d.Body))
			if nil != err {
				t.Error(err, "Failed to convert body to integer")
				t.FailNow()
			}
			log.Printf(" [.] Client got %d", res)
			break
		}
	}

	return

}

func startRpcServer(queueName string) {
	// https://www.rabbitmq.com/tutorials/tutorial-six-go.html

	// AMQP
	config := map[string]interface{}{
		"protocol": "amqp",
		"url":      "amqp://test:test@localhost:5672/",
		"secret":   "",
	}
	driver, err := mq.Build(config)
	if nil != err {
		return
	}

	// creates/get queue
	q, err := driver.QueueDeclare(map[string]interface{}{"name": queueName})
	if nil != err {
		return
	}
	fmt.Println("QUEUE STATUS: ", lygo_json.Stringify(q))

	listener, err := driver.NewListener(map[string]interface{}{"queue": queueName})
	if nil != err {
		return
	}
	err = listener.Listen(func(message map[string]interface{}) {
		correlationId := lygo_reflect.GetString(message, "correlation-id")
		replyTo := lygo_reflect.GetString(message, "reply-to")
		response := 1234567890
		n, err := strconv.Atoi(lygo_conv.ToString(message["body"]))
		if nil != err {
			fmt.Println(err, "Failed to convert body to integer")
		}
		fmt.Println("[*]", "RPC SERVER LISTENER", "correlation-id:", correlationId, "reply-to", replyTo, "message:", message["body"], lygo_json.Stringify(message))
		log.Printf(" [.] Server received(%d). Sending (%d).", n, response)
		mm := map[string]interface{}{
			"content-type":   "text/plain",
			"body":           []byte(fmt.Sprintf("%v", response)),
			"correlation-id": correlationId,
		}
		emitter, err := driver.NewEmitter(map[string]interface{}{"key": replyTo})
		if nil != err {
			fmt.Println("ERROR", err)
			return
		}
		err = emitter.Emit(mm)
		if nil != err {
			fmt.Println("ERROR", err)
			return
		}
	})
	if nil != err {
		return
	}
}

func startRpcNativeServer(queueName string) {
	// https://www.rabbitmq.com/tutorials/tutorial-six-go.html
	conn, err := amqp.Dial("amqp://test:test@localhost:5672/")
	if nil != err {
		fmt.Println(err, "Failed to connect to RabbitMQ")
		return
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if nil != err {
		fmt.Println(err, "Failed to open a channel")
		return
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if nil != err {
		fmt.Println(err, "Failed to declare a queue")
		return
	}

	err = ch.Qos(1, 0, false)
	if nil != err {
		fmt.Println(err, "Failed to set QoS")
		return
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if nil != err {
		fmt.Println(err, "Failed to register a consumer")
		return
	}

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			n, err := strconv.Atoi(string(d.Body))
			if nil != err {
				fmt.Println(err, "Failed to convert body to integer")
				continue
			}

			response := 1234567890
			log.Printf(" [.] Server received(%d). Sending (%d).", n, response)

			err = ch.Publish(
				"",        // exchange
				d.ReplyTo, // routing key
				false,     // mandatory
				false,     // immediate
				amqp.Publishing{
					ContentType:   "text/plain",
					CorrelationId: d.CorrelationId,
					Body:          []byte(strconv.Itoa(response)),
				})
			if nil != err {
				fmt.Println(err, "Failed to publish a message")
			}
			_ = d.Ack(false)
		}
	}()

	log.Printf(" [*] Awaiting RPC requests")
	<-forever

}
