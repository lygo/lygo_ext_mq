package _test

import (
	"bitbucket.org/lygo/lygo_ext_mq/mq/mq_drivers"
	"testing"
	"time"
)

func TestHandlerJoinTimeout(t *testing.T) {
	handler := mq_drivers.NewListenerAmqpHandler()
	handler.JoinTimeout(3*time.Second)
}
