module bitbucket.org/lygo/lygo_ext_mq

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	github.com/google/uuid v1.3.0 // indirect
	github.com/streadway/amqp v1.0.0 // indirect
)
