# Message Queue #
![icon](icon.png)

Message Queue implementation that support drivers for:
 - AMQP: RabbitMQ client 


### Versioning ###

Sources are versioned using git tags:

```
git tag v0.1.8
git push origin v0.1.8
```

Latest:

```
go get -u bitbucket.org/lygo/lygo_ext_mq@v0.1.8
```


## Dependencies ##

```
go get -u bitbucket.org/lygo/lygo_commons

go get -u github.com/streadway/amqp

```